const HDWalletProvider = require('@truffle/hdwallet-provider');
require('dotenv').config();

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      gas: 6721975,
      gasLimit: 6721975,
      gasPrice: 1,
    },
    bsc: {
      provider: () => new HDWalletProvider("", `https://bsc-dataseed1.binance.org`),
      network_id: 56,
    },
    ropsten: {
      network_id: "3",
      provider: () =>
          new HDWalletProvider(
            process.env.MNENOMIC,
            `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`,
          ),
      gasPrice: 10000000000, // 10 gwei
      gas: 6900000,
      //from: account, 
    },
    kovan: {
      network_id: "42",
      provider: () =>
          new HDWalletProvider(
            process.env.MNENOMIC,
            `https://kovan.infura.io/v3/${process.env.INFURA_API_KEY}`,
          ),
      gasPrice: 10000000000, // 10 gwei
      gas: 6900000,
      //from: account,
      timeoutBlocks: 500,
    },
    BSCTestnet: {
      network_id: "97",
      provider: () =>
          new HDWalletProvider(
            process.env.MNENOMIC,
            'https://data-seed-prebsc-1-s1.binance.org:8545/',
          ),
      gasPrice: 10000000000, // 10 gwei
      gas: 6900000,
      //from: account,
      timeoutBlocks: 500,
    },
  },

  compilers: {
    solc: {
      version: '0.6.6',
        docker: false,
        settings: {
          optimizer: {
            enabled: true,
            runs: 500   // Optimize for how many times you intend to run the code
          },
        },
    }
  },
  plugins: [
    "truffle-plugin-verify",
  ],
  api_keys: {
    bscscan: process.env.BSCSCAN_API_KEY
  }
};
